
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class pattern {

    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("[A-Za-z\\s]+(\\d+)+[a-z\\s]+(\\d+).*", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher("Mam kredyt 3000 zl odsetki to 200 zl");
        // boolean matchFound = matcher.find()
        boolean matchFound = matcher.matches();
        if (matchFound) {
            System.out.println(matcher.group(0));
            System.out.println(matcher.group(1));
            System.out.println(matcher.group(2));
        } else {
            System.out.println("Match not found");
        }
    }
}
